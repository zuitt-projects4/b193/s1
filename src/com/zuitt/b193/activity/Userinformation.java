package com.zuitt.b193.activity;

import java.util.Scanner;

public class Userinformation {

    public static void main(String[] args){

        Scanner appScanner = new Scanner(System.in);

        System.out.println("First Name: ");
        String first_name = appScanner.nextLine();
        System.out.println("Last Name: ");
        String last_name = appScanner.nextLine();
        System.out.println("First Subject Grade:");
        double first_grade = appScanner.nextDouble();
        System.out.println("Second Subject Grade:");
        double second_grade = appScanner.nextDouble();
        System.out.println("Third Subject Grade:");
        double third_grade = appScanner.nextDouble();

        System.out.println("Good day, " + first_name + " " + last_name +".");

        int average = (int)(first_grade + second_grade + third_grade) / 3;


        System.out.println("Your grade average is: " + average);
        
    }

}
